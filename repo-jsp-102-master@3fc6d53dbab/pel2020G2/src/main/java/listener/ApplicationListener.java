package listener;

import facade.FacadeParis;
import facade.FacadeParisStaticImpl;

import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationListener  implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        FacadeParis facade= new FacadeParisStaticImpl();
        servletContextEvent.getServletContext().setAttribute("facade",facade);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        servletContextEvent.getServletContext().setAttribute("facade",null);

    }
}
