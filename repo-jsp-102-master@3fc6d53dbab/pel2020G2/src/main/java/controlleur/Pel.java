package controlleur;

import facade.FacadeParis;
import facade.FacadeParisStaticImpl;
import facade.exceptions.InformationsSaisiesIncoherentesException;
import facade.exceptions.UtilisateurDejaConnecteException;
import modele.Match;
import modele.Utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

public class Pel extends HttpServlet {
    //action quelle doit faire en fonction de l'action
    public final static String HOME="home";
    public final static String LOGIN="login";
    public final static String LOGOUT="logout";
    public final static String GOMENU="goMenu";
    public final static String GOPARISOUVERTS="goParisOuverts";
    public final static String GOMESPARIS="goMesParis";
    public final static String PARIER="parier";
    public final static String VALIDERPARIS="validerParis";
    FacadeParis facade= null;
    Utilisateur utilisateur=null;

    @Override
    public void init() throws ServletException {
        facade= (FacadeParis) getServletContext().getAttribute("facade");

    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // definir une action
        String action = "HOME";
        String target = "/WEB-INF/pages/connexion.jsp";

        //passe par une url eg= connexion.action

        String pathInfo = req.getRequestURI().substring(req.getContextPath().length());
        //url demander n'est pas vide

        if (!(pathInfo == null || "".equals(pathInfo) || "/".equals(pathInfo))) {
            String[] all = pathInfo.split("[/\\.]");
            //normalement
            if ((all.length > 1) && ("action".equals(all[all.length - 1]))) {
                action = all[all.length - 2];
            }
        }

        if (LOGIN.equals(action)) {
            String login = req.getParameter("login");
            String psw = req.getParameter("mdp");
            String erreur = "";
            if (login == null || login.length() < 2) {
                erreur = "le login doit comporter au moins deux caracteres";
            } else {
                if (psw == null || psw.length() < 2) {
                    erreur = "le mot de passe  doit comporter au moins deux caracteres";
                }
                else {
                    try {

                        utilisateur = facade.connexion(login, psw);
                        req.getSession().setAttribute("utilisateur", utilisateur);
                        target = "/WEB-INF/pages/menu.jsp";
                    } catch (UtilisateurDejaConnecteException e) {
                        erreur = "utilisateur" + login + "deja connecter";
                    } catch (InformationsSaisiesIncoherentesException e) {
                        erreur = "couple pseudo password incohérent";
                    }

                }
            }
            req.setAttribute("erreur", erreur);
        }else {
            utilisateur= (Utilisateur) req.getSession().getAttribute("utilisateur");
        }if (utilisateur==null){
            target=target="/WEB-INF/pages/connexion.jsp";
        }else {
            switch (action){
                case LOGOUT:
                    facade.deconnexion(utilisateur.getLogin());
                    req.getSession().invalidate();
                    target="/WEB-INF/pages/connexion.jsp";
                    break;
                case GOPARISOUVERTS:
                    Collection<Match> parisOuverts=facade.getMatchsPasCommences();
                    req.setAttribute("parisOuverts",parisOuverts);
                    target="/WEB-INF/pages/pel/parisOuvert.jsp";
                    break;
                case GOMENU:
                    target="/WEB-INF/pages/pel/Menu.jsp";
                    break;
                case GOMESPARIS:
                    target="/WEB-INF/pages/pel/mesPAris.jsp";
                    break;
                case VALIDERPARIS:
                    target="/WEB-INF/pages/pel/Menu.jsp";
                    break;
                case PARIER:
                    long id=Long.parseLong(req.getParameter("idMatch"));
                    Match match=facade.getMatch(id);
                    target="/WEB-INF/pages/pel/parier.jsp";
                    break;
            }
        }

        getServletContext().getRequestDispatcher(target).forward(req,resp);
    }
}
