<%--
  Created by IntelliJ IDEA.
  User: sould
  Date: 21/01/2020
  Time: 21:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:useBean id="utilisateur" type="modele.Utilisateur" scope="session"></jsp:useBean>
    <jsp:useBean id="match" type="modele.Match" scope="request"></jsp:useBean>
</head>
<body>
Bienvenue <strong>${utilistateur.login}</strong>
<p> Vous voulez parier sur le match: ${match.equipe1} vs ${match.equipe2}
    le ${match.quand}

</p>
<form action="/pel/confirmationParis.action" method="post">
    <label>Verdict</label>
    <select name="verdict">
        <option value="${match.equipe1}">${match.equipe1}</option>
        <option value="${match.equipe2}">${match.equipe2}</option>
    </select>
    <label>Montant</label>
    <p>
        <input type="text" value="Parier">
    </p>
    <p>
        <input type="submit" value="Parier">
    </p>
</form>
<a href="/pel/goMenu.action">Retours au menu principal</a>
</body>
</html>
