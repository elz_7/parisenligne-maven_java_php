<%--
  Created by IntelliJ IDEA.
  User: sould
  Date: 21/01/2020
  Time: 21:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:useBean id="parisOuverts" type="java.util.Collection" scope="request"></jsp:useBean>
    <jsp:useBean id="utilisateur" type="modele.Utilisateur" scope="session"></jsp:useBean>
</head>
<body>
<strong align="center">Match ouvert aux paris </strong>
Bienvenue <strong>${utilistateur.login}</strong>

<ul>
    <c:forEach items="${parisOuverts}" var="match"></c:forEach>
    <li>
        Sport: ${match.sport} -${match.equipe1} vs ${match.equipe2} -
        <a href="/pel/parier.action" id="$match.idMatch">parier</a>
    </li>
</ul>
<li><a href="/pel/goMenu.action"> Menu principal  </a></li>
</body>
</html>